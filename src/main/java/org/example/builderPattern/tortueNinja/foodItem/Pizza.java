package org.example.builderPattern.tortueNinja.foodItem;

import org.example.builderPattern.tortueNinja.components.Ingredient;
import org.example.builderPattern.tortueNinja.components.Size;

import java.util.HashSet;

public class Pizza {
    private HashSet<Ingredient> garniture;
    private Size size;



    public Pizza(HashSet<Ingredient> garniture, Size size) {
        this.garniture = garniture;
        this.size = size;
    }

    public HashSet<Ingredient> getGarniture() {
        return garniture;
    }

    public void setGarniture(HashSet<Ingredient> garniture) {
        this.garniture = garniture;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "garniture=" + garniture +
                ", size=" + size +
                '}';
    }
}
