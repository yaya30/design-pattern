package org.example.builderPattern.tortueNinja.components;

public enum Size {
    SMALL, MEDIUM, LARGE
}
