package org.example.builderPattern.tortueNinja.components;

public enum Ingredient {
    PEPPERONNI, FROMAGE, TOMATO, MUSHROOM
}
