package org.example.builderPattern.tortueNinja.director;

import org.example.builderPattern.tortueNinja.builders.PizzaBuilder;
import org.example.builderPattern.tortueNinja.components.Ingredient;
import org.example.builderPattern.tortueNinja.components.Size;

import java.util.List;
import java.util.function.Consumer;

public class PizzaDirector {
    private void createGarniture(PizzaBuilder pizzaBuilder, List<Ingredient> ingredients){
        if( !ingredients.isEmpty()){
            Consumer<Ingredient> ingredientConsumer = pizzaBuilder::addIngredient;
            ingredients.forEach(ingredientConsumer);
        }
    }
    public void constructPersonnalizedPizza(PizzaBuilder pizzaBuilder, Size size, List<Ingredient> ingredients){
        if (size != null) {
            pizzaBuilder.setSize(size);
        }
        if(ingredients != null){
            createGarniture(pizzaBuilder, ingredients);
        };
    }
    public void constructPizzaWithoutGarniture(PizzaBuilder pizzaBuilder, Size size){
        if (size != null) {
            pizzaBuilder.setSize(size);
        }
    }
}
