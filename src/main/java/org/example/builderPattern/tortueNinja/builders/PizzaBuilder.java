package org.example.builderPattern.tortueNinja.builders;

import org.example.builderPattern.tortueNinja.foodItem.Pizza;
import org.example.builderPattern.tortueNinja.components.Ingredient;
import org.example.builderPattern.tortueNinja.components.Size;

import java.util.HashSet;

public  class PizzaBuilder implements FoodBuilderProcess {
    private HashSet<Ingredient> garniture;
    private Size size;

    public PizzaBuilder() {
        this.garniture = new HashSet<>();
        this.size = Size.MEDIUM; // value by default
    }

    @Override
    public void addIngredient(Ingredient ingredient) {
        this.garniture.add(ingredient);
    }

    @Override
    public void setSize(Size size) {
        this.size = size;
    }
    public Pizza build() {
        Pizza newPizza = new Pizza(garniture, size);
        garniture = new HashSet<>();
        size = Size.MEDIUM;
        return newPizza;
    }
}
