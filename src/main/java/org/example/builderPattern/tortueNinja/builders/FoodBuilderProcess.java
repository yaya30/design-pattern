package org.example.builderPattern.tortueNinja.builders;

import org.example.builderPattern.tortueNinja.components.Ingredient;
import org.example.builderPattern.tortueNinja.components.Size;

public interface FoodBuilderProcess {
    void addIngredient(Ingredient ingredient);
    void setSize(Size size);
}
