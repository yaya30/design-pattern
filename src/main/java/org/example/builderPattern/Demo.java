package org.example.builderPattern;

import org.example.builderPattern.tortueNinja.builders.PizzaBuilder;
import org.example.builderPattern.tortueNinja.foodItem.Pizza;
import org.example.builderPattern.tortueNinja.components.Ingredient;
import org.example.builderPattern.tortueNinja.components.Size;
import org.example.builderPattern.tortueNinja.director.PizzaDirector;

import java.util.Arrays;

public class Demo {
    public static void main(String[] args) {
        PizzaDirector pizzaDirector = new PizzaDirector();
        PizzaBuilder pizzaBuilder = new PizzaBuilder();
        // Pizza Leonardo
        pizzaDirector.constructPersonnalizedPizza(
                pizzaBuilder,
                Size.LARGE,
                Arrays.asList(
                        Ingredient.FROMAGE,
                        Ingredient.PEPPERONNI)
        );
        Pizza pizzaLeonardo = pizzaBuilder.build();
        System.out.println(pizzaLeonardo.toString());

        // Pizza Michelangelo
        pizzaDirector.constructPersonnalizedPizza(
                pizzaBuilder,
                Size.SMALL,
                Arrays.asList(
                        Ingredient.PEPPERONNI,
                        Ingredient.MUSHROOM)
        );
        Pizza pizzaMichelangelo = pizzaBuilder.build();
        System.out.println(pizzaMichelangelo.toString());

        // Pizza Michelangelo
        pizzaDirector.constructPizzaWithoutGarniture(
                pizzaBuilder,
                Size.SMALL
        );
        Pizza pizzaRaphaelo = pizzaBuilder.build();
        System.out.println("raphaelo" +pizzaRaphaelo.toString());

        // Pizza Donatello
        pizzaDirector.constructPersonnalizedPizza(
                pizzaBuilder,
                Size.MEDIUM,
                Arrays.asList(
                        Ingredient.FROMAGE,
                        Ingredient.PEPPERONNI,
                        Ingredient.MUSHROOM
                )
        );
        Pizza pizzaDonatello = pizzaBuilder.build();
        System.out.println(pizzaDonatello.toString());
    }
}
