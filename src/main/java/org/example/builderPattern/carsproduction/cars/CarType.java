package org.example.builderPattern.carsproduction.cars;

public enum CarType {
    CITY_CAR, SPORTS_CAR, SUV
}
